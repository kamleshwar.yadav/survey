@extends('layouts.app')
@section('content')
<div class=" mt-5">
	@include('partial.alert')
	<table class="table table-bordered table-responsive">
		<thead>
			<tr>
				<th>#</th>
				<th>State</th>
				<th>Discom</th>
				<th>Type of Consumer</th>
				<th>Consumer Name</th>
				<th>Consumer Address</th>
				<th>Consumer Supply Type</th>
				<th>Complaint Type Of Consumer</th>
				<th>Complaint First Level</th>
				<th>Complaint Email</th>
				<th>Complaint Contact Number</th>
				<th>Complaint Office Address</th>
				<th>Complaint Escalation</th>
				<th>Complaint Action</th>
				<th>Complaint Method</th>
				<th>Complaint No. of days taken</th>
				<th>Complaint Satisfied</th>
				<th>Complaint Remarks</th>
				<th>Complaint Date</th>
			</tr>
		</thead>
		<tbody>
			@forelse($list as $row)
				<tr>
					<td>{{$loop->index+1}}</td>
					<td>{{$row->state}}</td>
					<td>{{$row->discom}}</td>
					<td>{{$row->type_of_consumer}}</td>
					<td>{{$row->consumer_name}}</td>
					<td>{{$row->consumer_address}}</td>
					<td>{{$row->consumer_supply_type}}</td>
					<td>{{$row->complaint_type_of_consumer}}</td>
					<td>{{$row->complaint_first_level}}</td>
					<td>{{$row->complaint_email}}</td>
					<td>{{$row->complaint_contact_number}}</td>
					<td>{{$row->complaint_office_address}}</td>
					<td>{{$row->complaint_escalation}}</td>
					<td>{{$row->complaint_action_require}}</td>
					<td>{{$row->complaint_method_approaching}}</td>
					<td>{{$row->complaint_no_of_days_taken}}</td>
					<td>{{$row->complaint_satisfied}}</td>
					<td>{{$row->complaint_other_remarks}}</td>
					<td>{{date('d/M/Y',strtotime($row->created_at))}}</td>
				</tr>
			@empty
			@endforelse
		</tbody>
	</table>
</div>
@endsection