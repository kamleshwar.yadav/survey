@extends('layouts.app')
@section('content')
<div style="width: 61%;height: 80%;relative: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;">
	@include('partial.alert')
    <div class="card">
        <div class="card-header">
            Profile of the consumer
        </div>
        <div class="card-body">
            <form class="" action="{{route('survey.store')}}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputState">State</label>
                        <select id="inputState" class="form-control @error('state') is-invalid @enderror" name="state"> 
                            <option value="">Select State</option>
                            <option>Delhi</option>
                            <option>Utter Pradesh</option>
                        </select>
                        @error('state')
                        	<div class="invalid-feedback">
		                        {{ $message }}
		                    </div>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputDiscom">Discom</label>
                        <select id="inputDiscom" class="form-control @error('discom') is-invalid @enderror" name="discom">
                            <option value="">Select Discom</option>
                            <option>Discom1</option>
                            <option>Discom2</option>
                        </select>
                        @error('discom')
                        	<div class="invalid-feedback">
		                        {{ $message }}
		                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputDiscom">Type of consumer</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type_of_consumer" id="Rural" value="Rural" @if(old('type_of_consumer')=='Rural') checked  @endif/>
                            <label class="form-check-label" for="Rural">Rural</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="type_of_consumer" id="Urban" value="Urban" @if(old('type_of_consumer')=='Urban') checked  @endif/>
                            <label class="form-check-label" for="Urban">Urban</label>
                        </div>
                        @error('type_of_consumer')
                        	<br>
                        	<small class="text-danger">
		                        {{ $message }}
		                    </small>
                        @enderror
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="consumer_name">Name</label>
                        <input type="text" class="form-control @error('consumer_name') is-invalid @enderror" id="consumer_name" placeholder="Name" name="consumer_name" value="{{old('consumer_name')}}" />
                        @error('consumer_name')
                        	<div class="invalid-feedback">
		                        {{ $message }}
		                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <textarea  class="form-control @error('consumer_address') is-invalid @enderror" id="inputAddress" placeholder="Gurugram" name="consumer_address">{{old('consumer_address')}}</textarea>
                    @error('consumer_address')
                    	<div class="invalid-feedback">
	                        {{ $message }}
	                    </div>
                    @enderror
                </div> 
                <div class="form-group">
                    <label for="supply_type">Supply Type</label>
                    <select id="supply_type" class="form-control @error('consumer_supply_type') is-invalid @enderror " name="consumer_supply_type">
                        <option value="">Select Supply Type</option>
                        <option>Domestic</option>
                        <option>Commercial</option>
                        <option>Industrial</option>
                        <option>Agricultural</option>
                    </select>
                    @error('consumer_supply_type')
                    	<div class="invalid-feedback">
	                        {{ $message }}
	                    </div>
                    @enderror
                </div>                  
                                
                <div class="card-header">
                    Complaint/Grievance Details
                </div>
                <br>
                <div class="form-group">
                    <label for="supply_type">Type of consumer</label>
                    <select id="supply_type" class="form-control @error('complaint_type_of_consumer') is-invalid @enderror" name="complaint_type_of_consumer">
                        <option value="">Select Type of consumer</option>
                        <option>Domestic</option>
                        <option>Commercial</option>
                        <option>Industrial</option>
                        <option>Agricultural</option>
                    </select>
                    @error('complaint_type_of_consumer')
                    	<div class="invalid-feedback">
	                        {{ $message }}
	                    </div>
                    @enderror
                </div>  
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="first_level_of_grievance">1st level of the grievance redressal mechanism Name</label>
                        <input type="text" class="form-control @error('first_level_of_grievance') is-invalid @enderror" id="first_level_of_grievance" placeholder="Name" name="first_level_of_grievance" value="{{old('first_level_of_grievance')}}" />
                        @error('first_level_of_grievance')
                        	<div class="invalid-feedback">
    	                        {{ $message }}
    	                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="complaint_email">Email</label>
                        <input type="text" class="form-control @error('complaint_email') is-invalid @enderror" id="complaint_email" placeholder="Email" name="complaint_email" value="{{old('complaint_email')}}" />
                        @error('complaint_email')
                        	<div class="invalid-feedback">
    	                        {{ $message }}
    	                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="complaint_contact_number">Contact Number</label>
                        <input type="text" class="form-control @error('complaint_contact_number') is-invalid @enderror" id="complaint_contact_number" placeholder="Contact Number" name="complaint_contact_number" value="{{old('complaint_contact_number')}}" />
                        @error('complaint_contact_number')
                        	<div class="invalid-feedback">
    	                        {{ $message }}
    	                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="complaint_office_address">Office Address</label>
                        <textarea type="text" class="form-control @error('complaint_office_address') is-invalid @enderror" id="complaint_office_address" placeholder="Office Address" name="complaint_office_address">{{old('complaint_office_address')}}</textarea>
                        @error('complaint_office_address')
                        	<div class="invalid-feedback">
    	                        {{ $message }}
    	                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="complaint_escalation">Is the escalation matrix available to consumer </label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="complaint_escalation" id="complaint_escalation_Yes" value="Yes" @if(old('complaint_escalation')=='Yes') checked @endif />
                        <label class="form-check-label" for="complaint_escalation_Yes">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="complaint_escalation" id="complaint_escalation_No" value="No" @if(old('complaint_escalation')=='No') checked @endif />
                        <label class="form-check-label" for="complaint_escalation_No">No</label>
                    </div>
                    @error('complaint_escalation')
                    	<div class="text-danger">
	                        {{ $message }}
	                    </div>
                    @enderror
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="any-action-required">Any action required</label>
                        <textarea class="form-control @error('complaint_action') is-invalid @enderror" id="any-action-required" placeholder="Any action required" name="complaint_action">{{old('complaint_action')}}</textarea>
                        @error('complaint_action')
                        	<div class="invalid-feedback">
    	                        {{ $message }}
    	                    </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="complaint_method_of_approaching">Method of approaching</label>
                    <select id="complaint_method_of_approaching" class="form-control @error('complaint_method_of_approaching') is-invalid @enderror" name="complaint_method_of_approaching">
                        <option value="">Select Method of approaching</option>
                        <option>Phone</option>
                        <option>Physical Meeting</option>
                        <option>Email</option>
                        <option>Letter</option>
                    </select>
                    @error('complaint_method_of_approaching')
                    	<div class="invalid-feedback">
	                        {{ $message }}
	                    </div>
                    @enderror
                </div> 
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="no-of-days-taken">No. of days taken</label>
                        <input type="number" class="form-control @error('complaint_number_of_days_taken') is-invalid @enderror" id="no-of-days-taken" placeholder="No. of days taken" min="0" name="complaint_number_of_days_taken" value="{{old('complaint_number_of_days_taken')}}" />
                        @error('complaint_number_of_days_taken')
	                    	<div class="invalid-feedback">
		                        {{ $message }}
		                    </div>
	                    @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="Satisfied">Satisfied</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="complaint_satisfied" id="complaint_satisfied_yes" value="Yes" @if(old('complaint_satisfied')=='Yes') checked  @endif/>
                        <label class="form-check-label" for="complaint_satisfied_yes">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="complaint_satisfied" id="complaint_satisfied_no" value="No" />
                        <label class="form-check-label" for="complaint_satisfied_no">No</label>
                    </div>
                    @error('complaint_satisfied')
                    	<br>
                    	<small class="text-danger">
	                        {{ $message }}
	                    </small>
                    @enderror
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">                           
                        <label for="other-steps">Other Remarks</label>
                        <textarea type="text" class="form-control @error('complaint_other_remarks') is-invalid @enderror" id="other-Remarks" placeholder="Other Remarks" name="complaint_other_remarks">{{old('complaint_other_remarks')}}</textarea>
                        @error('complaint_other_remarks')
	                    	<div class="invalid-feedback">
		                        {{ $message }}
		                    </div>
	                    @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Submit</button>
            </form>
            
        </div>
    </div>
</div>

@endsection
