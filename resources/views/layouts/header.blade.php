<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	</button>
	
	<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
	        <li class="nav-item">
	            <a class="nav-link" href="{{route('home')}}">Dashboard <span class="sr-only">(current)</span></a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="{{route('survey.create')}}">Add New Survey <span class="sr-only">(current)</span></a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="{{route('survey.list')}}">My Survey <span class="sr-only">(current)</span></a>
	        </li>
	        <li class="nav-item">
	            <a class="nav-link" href="javascript:void(0)" onclick="document.getElementById('logout-form').submit();">Logout</a>
	        </li>
	        <form action="{{route('logout')}}" id="logout-form" method="POST">@csrf</form>
	        
	    </ul>
	    {{-- <form class="form-inline my-2 my-lg-0">
	        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
	        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
	    </form> --}}
	</div>
</nav>