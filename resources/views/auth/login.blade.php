@extends('layouts.layout')

@section('content')
    <div class="card-body">
        <h4 class="card-title">{{ __('Login') }}</h4>
        <form method="POST" action="{{ route('login') }}" class="my-login-validation" novalidate="">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('Email Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus >
                @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="password">{{ __('Password') }}
                    <a href="{{ route('password.request') }}" class="float-right">
                        Forgot Password?
                    </a>
                </label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  data-eye>
                @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <div class="custom-checkbox custom-control">
                    <input type="checkbox" name="remember" id="remember" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}>
                    <label for="remember" class="custom-control-label">Remember Me</label>
                </div>
            </div>

            <div class="form-group m-0">
                <button type="submit" class="btn btn-primary btn-block">
                    Login
                </button>
            </div>
            <div class="mt-4 text-center">
                Don't have an account? <a href="#">Create One</a>
            </div>
        </form>
    </div>
@endsection

