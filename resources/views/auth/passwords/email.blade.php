@extends('layouts.layout')

@section('content')
    <div class="card-body">
        <h4 class="card-title">{{ __('Reset Password') }}</h4>
        <form method="POST" action="{{ route('password.email') }}" class="my-login-validation" novalidate="">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('Email Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus >
                @error('email')
                    <div class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
            </div>
            <div class="form-group m-0">
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
            <div class="mt-4 text-center">
                Already have an account? <a href="{{route('login')}}">Login</a>
            </div>
        </form>
    </div>
@endsection
