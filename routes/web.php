<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SurveyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('survey',[SurveyController::class,'surveyList'])->name('survey.list');
Route::get('survey/create',[SurveyController::class,'surveyCreate'])->name('survey.create');
Route::post('survey',[SurveyController::class,'storeSurvey'])->name('survey.store');
