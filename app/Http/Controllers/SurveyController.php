<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Survey;
use Auth;

class SurveyController extends Controller
{
    public $survey;

    public function __construct(Survey $survey){
        $this->middleware('auth');
        $this->survey = $survey;
    }
    
    public function surveyList(){
        $list = $this->survey->where('user_id',Auth::id())->get();
        return view('survey.list',compact('list'));
    }

    public function surveyCreate(){
        return view('survey.create');
    }

    public function storeSurvey(Request $request){
        $this->validate($request,[
            'state'=>'required',
            'discom'=>'required',
            'type_of_consumer'=>'required',
            'consumer_name'=>'required',
            'consumer_address'=>'required',
            'consumer_supply_type'=>'required',
            'complaint_type_of_consumer'=>'required',
            'first_level_of_grievance'=>'required',
            'complaint_email'=>'required|email',
            'complaint_contact_number'=>'required|max:15',
            'complaint_office_address'=>'required',
            'complaint_escalation'=>'required',
            'complaint_action'=>'required',
            'complaint_method_of_approaching'=>'required',
            'complaint_number_of_days_taken'=>'required',
            'complaint_satisfied'=>'required',
            'complaint_other_remarks'=>'required',
        ]);
        
        try {
            $survey = new $this->survey;
            $survey->user_id = Auth::id();
            $survey->state = $request->state;
            $survey->discom = $request->discom;
            $survey->type_of_consumer = $request->type_of_consumer;
            $survey->consumer_name = $request->consumer_name;
            $survey->consumer_address = $request->consumer_address;
            $survey->consumer_supply_type = $request->consumer_supply_type;
            $survey->complaint_type_of_consumer = $request->complaint_type_of_consumer;
            $survey->complaint_first_level  = $request->first_level_of_grievance;
            $survey->complaint_email  = $request->complaint_email;
            $survey->complaint_contact_number  = $request->complaint_contact_number;
            $survey->complaint_office_address  = $request->complaint_office_address;
            $survey->complaint_escalation  = $request->complaint_escalation;
            $survey->complaint_action_require   = $request->complaint_action;
            $survey->complaint_method_approaching   = $request->complaint_method_of_approaching;
            $survey->complaint_no_of_days_taken   = $request->complaint_number_of_days_taken;
            $survey->complaint_satisfied   = $request->complaint_satisfied;
            $survey->complaint_other_remarks   = $request->complaint_other_remarks;
            $survey->save();
            session()->flash('success','Your survey query has been created successfully.');
            return redirect(route('survey.list'));
        } catch (\Exception $e) {
            session()->flash('error',$e->getMessage());
            return back()->withInput();
        }
        
    }
}
