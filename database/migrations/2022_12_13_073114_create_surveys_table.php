<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('state');
            $table->string('discom');
            $table->string('type_of_consumer');
            $table->string('consumer_name');
            $table->string('consumer_address');
            $table->string('consumer_supply_type');
            $table->string('complaint_type_of_consumer');
            $table->string('complaint_first_level');
            $table->string('complaint_email');
            $table->string('complaint_contact_number');
            $table->string('complaint_office_address');
            $table->string('complaint_escalation');
            $table->string('complaint_action_require');
            $table->string('complaint_method_approaching');
            $table->string('complaint_no_of_days_taken');
            $table->string('complaint_satisfied');
            $table->string('complaint_other_remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
